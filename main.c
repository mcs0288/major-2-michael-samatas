#include <stdio.h>
#include <unistd.h>
#include<stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <fcntl.h>

#define READ 0
#define WRITE 1

char *CommandLineProcessing(char *in, char *delimit, char *go, char *stop);//function to Processing the command line

int main(int argc , char *argv[]){
	FILE *f;//opining the file
	char *tok;//toking the  file
	char opener[] = {"\"\'"};// what to look for while Processing the command line
	char closer[] = {"\"\'"};// what to look for while Processing the command line
	char str[100];// the original string to get the command like
	char *strArr[200];// the for while Processed command line
	char strFinal[200];// the parsed command line after being pipes
	int num;// a counter to count how many arguments there are
	int looper = 1;// loop untill file ends
	int noFile = 0;// chech if there is a file
	int ID;// is for the fork
	int fd[2], nbytes;// pipe 
	ID = fork();//fork
	pipe(fd);//pipe

	if(argc == 1){//if there is no file make the varuble 1
        	noFile = 1;
        }
	else if(argc > 1){// if there is a file open the file
                f = fopen(argv[1], "r");
                if(f == NULL){
                        perror(argv[1]);
                        exit(1);
                }
	}
        else{//exit
                exit(1);
        }

	while(looper == 1){//loop
		if(noFile == 1){//if there is no file take input from user
                	printf("&");
                        fgets(str, sizeof(str), stdin);
                }
                else{//if there is a file take input from file
                        fscanf(f, "%s", str);
                }

                tok = CommandLineProcessing(str, " \t\a\r\n", opener, closer);//call Processing function
                strArr[0] = tok;//put the tok intothe array

                num = 1;

                while((tok = CommandLineProcessing(NULL, " \t\a\r\n", opener, closer)) != NULL){//call Processing function
                	strArr[num] = tok;//put the tok intothe array
                       	num++;
                }
	
		if(ID < 0){//if ID is < 0 esit
                        perror("pipe");
                        exit(1);
                }
        	if(ID == 0){//child
			write(fd[0], strArr, sizeof(strArr));// pipe array to parent
        	}
		else{//parent
			read(fd[1], strFinal, sizeof(strFinal));// get array from child

			if(num >= 2){
                                execlp(strArr[0], strArr[0], strArr[1], (char *)0);//do the command
                        }
                        else{
                                execlp(strArr[0], strArr[0], (char *)0);// do the command
                        }
        	}
	}
return 0;
}

char *CommandLineProcessing (char *in, char *delimit, char *go, char *stop){//function to Processing the command line
        static char *token = NULL;//token the words
        char *head = NULL;//strat of the work
        char *tail = NULL;//end of the word
        int counter = 0;//counter
        int counterLoc = 0;//counter lock

        if(in != NULL){//if there string is not empty
                token = in;//put it in the token char
                head = in;//put it in the head char
        }
        else{
                head = token;//if emtys 
                if (*token == '\0'){//token is nuLL
                        head = NULL;//head is NULL
                }
        }
        while(*token != '\0'){//while token is not empty 
                if(counter){
                        if(stop[counterLoc] == *token){//if stop word is fount
                                counter = 0;//counter = 0
                        }
                        token++;
                }
                if((tail = strchr(go, *token)) != NULL){//if starter matches the token 
                        counter = 1;//counter = 1
                        counterLoc = tail - go;
                        token++;
                }
                if(strchr(delimit, *token) != NULL){//if the delimiter matches the token 
                        *token = '\0';//token = Null
                        token++;//token++
                        break;
                }
                token++;
        }
return head;
}
